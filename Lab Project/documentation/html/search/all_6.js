var searchData=
[
  ['generatecomments',['generateComments',['../classnec__backend_1_1_global.html#a7c91dbc8a2cb98d68e8f9f1338be5370',1,'nec_backend.Global.generateComments()'],['../classnec__backend_1_1_structure.html#a74bbfc3e7d793ce553adfd76c632f287',1,'nec_backend.Structure.generateComments()']]],
  ['generateexecution',['generateExecution',['../classnec__backend_1_1_global.html#a1606bdec1bbadb23a0a5b2f400c268cb',1,'nec_backend.Global.generateExecution()'],['../classnec__backend_1_1_structure.html#a345d9a77277d4d4dafbcf30a3503b0ed',1,'nec_backend.Structure.generateExecution()']]],
  ['generategeometry',['generateGeometry',['../classnec__backend_1_1_structure.html#ae059a58fcbeb6a57fa91f01966579f73',1,'nec_backend::Structure']]],
  ['generatenec',['generateNEC',['../classnec__backend_1_1_nec_generator.html#ac00f4d94a538b1a0b4f42feb13d1e908',1,'nec_backend::NecGenerator']]],
  ['getlength',['getLength',['../classnec__backend_1_1_helper.html#acf6e33d8fb2aedc8f0d9b41828cc3f60',1,'nec_backend.Helper.getLength(double x1, double y1, double z1, double x2, double y2, double z2)'],['../classnec__backend_1_1_helper.html#af8807e814286332d60a5798e47c3f89d',1,'nec_backend.Helper.getLength(double[] pos1, double[] pos2)']]],
  ['getstring',['getString',['../classnec__backend_1_1_helper.html#a22d96d93559474998523b44e65dcbd6e',1,'nec_backend.Helper.getString(String[] inStr, int from)'],['../classnec__backend_1_1_helper.html#a69f20848b23ca4239c824a635dd20779',1,'nec_backend.Helper.getString(String[] inStr)']]],
  ['getstructurelabels',['getStructureLabels',['../classnec__backend_1_1_nec_generator.html#a685d04b840d26536bc2fe129912b345f',1,'nec_backend::NecGenerator']]],
  ['getstructurenames',['getStructureNames',['../classnec__backend_1_1_nec_generator.html#a74cb9f8dc658809eece3b09afb8af256',1,'nec_backend::NecGenerator']]],
  ['getstructureparameters',['getStructureParameters',['../classnec__backend_1_1_nec_generator.html#afb16e5b7c1a7c74349b27422449af6a4',1,'nec_backend::NecGenerator']]],
  ['getstructurerules',['getStructureRules',['../classnec__backend_1_1_nec_generator.html#aba077edc13331ae988e4fd9e8e628cc1',1,'nec_backend::NecGenerator']]],
  ['getunitvector',['getUnitVector',['../classnec__backend_1_1_helper.html#a5dbb6558d9f8d7166d055a71ba9dbe31',1,'nec_backend.Helper.getUnitVector(double x1, double y1, double z1, double x2, double y2, double z2)'],['../classnec__backend_1_1_helper.html#afbb2bf66f03f3c22af4d15f522b6e0c5',1,'nec_backend.Helper.getUnitVector(double[] pos1, double[] pos2)']]],
  ['global',['Global',['../classnec__backend_1_1_global.html#a2d3c17c91676fafdfebea9d399d4ecf9',1,'nec_backend::Global']]],
  ['global',['Global',['../classnec__backend_1_1_global.html',1,'nec_backend']]]
];
