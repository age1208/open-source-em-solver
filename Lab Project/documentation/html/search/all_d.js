var searchData=
[
  ['set_5fparameters',['set_parameters',['../classnec__backend_1_1_structure.html#a9498d2a6300738c2e797c93490ddffe0',1,'nec_backend::Structure']]],
  ['setfrequency',['setFrequency',['../classnec__backend_1_1_global.html#ae5ef26353d13c0bfa692700967ac5d8f',1,'nec_backend.Global.setFrequency(double f_low)'],['../classnec__backend_1_1_global.html#a6a4721e228be178be3db923b2f1f20f2',1,'nec_backend.Global.setFrequency(double f_low, double f_high, boolean f_isMultiplicative, int f_noSteps)']]],
  ['setground',['setGround',['../classnec__backend_1_1_global.html#aa2cefb5a2f3706617401231a1ef21684',1,'nec_backend.Global.setGround(boolean isGround)'],['../classnec__backend_1_1_global.html#aba5111024d8190fbccd730fa0b717aa2',1,'nec_backend.Global.setGround(double dielectricConstant, double conductivity)']]],
  ['solveline',['solveLine',['../classnec__backend_1_1_helper.html#a4bee3b8b7a312eacb7ed42166ff8eed6',1,'nec_backend::Helper']]],
  ['structure',['Structure',['../classnec__backend_1_1_structure.html',1,'nec_backend']]],
  ['structure',['Structure',['../classnec__backend_1_1_structure.html#af84b1d788d9b013d76fc0fb230bc9137',1,'nec_backend::Structure']]],
  ['structuretest',['StructureTest',['../class_structure_test.html',1,'']]],
  ['substitutevariable',['substituteVariable',['../classnec__backend_1_1_helper.html#a1c7a8ae1ce53e5285d0c010951e5f11d',1,'nec_backend::Helper']]],
  ['substitutevariabled',['substituteVariableD',['../classnec__backend_1_1_helper.html#a667689849b1f0d94a7ee8cd2afa761f2',1,'nec_backend::Helper']]]
];
