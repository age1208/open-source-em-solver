\documentclass[10pt,a4paper]{IEEEtranA4}
\addtolength{\hoffset}{-2mm}
\usepackage{blindtext}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{pdfpages}
\usepackage{float}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{flushend}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows}
\usepackage{multirow}
\usepackage{pgfgantt}
\usepackage{rotating}
\usepackage{pdflscape}
\usepackage{pdfpages}
\usepackage{tablefootnote}
\usepackage[space]{cite}

%
% PDF Info
%
%\ifpdf
%\pdfinfo{
%/Title (COST EFFECTIVE JOGGING SPEED MEASUREMENT PROJECT)
%/Author (Adriano De Gouveia)
%/CreationDate (D:201406151200)
%/Subject (ELEN3017 Final Practical Design Report, 2014)
%/Keywords (not important)
%}
%\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\onecolumn
\title{Open Source Electromagnetic Solver Project Proposal}
\author{Nathan Jordaan (597205), Partner: Adriano De Gouveia (605159) \\ ELEN4002 - Electrical Engineering Laboratory \\ July 20, 2015 \thanks{School of Electrical \& Information Engineering, University of the Witwatersrand, Private Bag 3, 2050, Johannesburg, South Africa}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

\maketitle
\thispagestyle{empty}
\begin{abstract}
This paper proposes the construction of an intuitive open source electromagnetic (EM) solver that can be used for teaching purposes. An investigation into existing free solvers is also proposed, this will aid the initial proposal. The solver will be characterised by an intuitive Graphical User Interface (GUI), as well as features that will promote understanding in the field of electromagnetics. The solvers will focus on wire antennae.
\end{abstract}
%\vfill
%\begin{figure*}[h] \centering
%	\includegraphics{include/WitsEIE-logo-colour.png}
%\end{figure*}
%\vfill

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%BEGIN OF BODY%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage \setcounter{page}{1}
\twocolumn
\section{INTRODUCTION}
\label{sec:introduction}
Electromagnetic (EM) solvers are important in solving Maxwell's equations in many electronic and electrical systems. These programs solve for electric E-fields and magnetic H-fields around antennas (radiating current carrying conductors). Even a conductor not designed to be antenna, still radiates like one. This can cause huge problems in electronics and antenna systems based on the surroundings. Solving Maxwell's equations by hand is nearly impossible for all but the simplest of cases -- and even difficult then from first principles since the E and H fields are coupled. Without a good intuitive understanding of Maxwell's equations and electromagnetics in general, one could not use these programs easily. The inexperienced user also runs the risk of poorly interpreting the parameters. EM solvers are discussed further in Section \ref{sec:background}.

In the interest of education, it is proposed that an easy-to-use open source EM solver needs to be discovered or built. This is because many free packages are difficult to use, some even being completely text based. SuperNEC, a previous package used, became outdated because it was dependant on MATLAB and would require updates for MATLAB updates. It was also not open which meant that once the owners lost interest, it would become obsolete -- which it did. The requirements, assumptions and constraints for this solver and survey of existing packages are laid out in Section \ref{sec:problem}. The proposed manner in which the project will be completed is then covered in Section \ref{sec:methodology}. It is a methodology for both the software evaluation and building the open source solver. Project timeline, required resources and division of labour is covered in Section \ref{sec:projmang}.

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
\section{BACKGROUND UNDERSTANDING}
\label{sec:background}

\subsection{Existing Solutions}
\label{existsolns}
Many computational EM solvers exist, both free and paid. In the interest of education, and sales, many commercial (paid) solvers have limited versions that can be used on trial or by students. However, there are still a number of free (or relatively cheap) solvers, the comparative strengths and weaknesses of them largely unknown. All solvers use a computational method (or combination of), that takes a approximate approach solving for Maxwell's equations.

	\subsubsection{Computational Methods}
	A number of numerical methods have been developed as computational electromagnetics has become extremely viable due to increased processing power. Each of the methods provide their own strengths and weaknesses and are therefore suitable for their own applications\cite{5156093,fekonumericalmethods}. 
	
	Methods of Moments (MoM) is a method applicable to electrically small, conductive, radiating structures. It is a full wave frequency-domain solution of Maxwell's equations. Finite Difference Time Domain (FDTD) would be another viable option for radiating structures, but is inferior in modelling thin wires. Finite Element Method (FEM) is another package which is also less capably of thin wire modelling and is more suited to wave-guide problems. There are many more methods, but they are not applicable for wire antennae.
	
	
	\subsubsection{Software Packages}
	There are software packages available, both commercial and free, which make use of these computational methods. Some of these packages combine methods through hybridisation, enabling a greater diversity of problems that can be solved\cite{fekonumericalmethods}. A Graphical User Interface (GUI) is a major benefit for modelling as it greatly increases usability. Leading commercial software includes extensive hybridisation, parallel processing and good usability through GUIs. Free programs often have no or poor GUIs, making the learning curve steep. 
	
	Let us consider a commercial software package, FEKO. FEKO does not lose its power through limitations in the user interface since Lua scripts can be run. However, the user interface is still well developed. Figure \ref{fig:isometric} shows a simulated 3D radiation pattern for a dipole near an undriven element simulated in FEKO. It is an example of a radiation pattern of a wire antenna generated using physical dimensions and the frequency of the source. FEKO LITE does not allow the user to solve a 3D pattern like this due to the restricted number of points. With fewer points a user could simulate the azimuthal and elevation plane independently. FEKO LITE also requires registration for use, and then only allows for a single month of use. This is not suitable for the needs for the general student or user. However, FEKO is a good benchmark for what a good electromagnetic solver can be. It allows for the user to build antennae graphically using a CAD interface and generates intuitive radiation patterns in 3D. This program, for all its power can be tricky to figure out for an inexperienced user. However, FEKO has provided video tutorials and pre-built examples for users to learn.
	
\begin{figure}[h] \centering
	\includegraphics[width=\columnwidth]{include/isometric.png}
	\caption{Isometric view of a driven dipole next to a parasitic element radiation pattern modelled in FEKO\label{fig:isometric}}
\end{figure}

\subsection{Technology Required}
\label{tech}
The open source EM solver to be built requires an existing numerical method engine as a backend. A graphical front end will most likely need to be coded from scratch (unless an open source one already exists). The front end should be able to offer some functionality found in existing packages.

One of several languages could be used (with additional APIs or libraries) for developing the front end. The following have been considered:
\begin{itemize}
	\item Java with a GUI library such as Swing or JavaFX
	\item Python with a GUI framework most likely the built in tkinter library
	\item C++ with the Qt library for cross-platform graphical user interfacing
	\item Ruby + Shoes GUI library 
\end{itemize}
A 3D rendering engine will also be required, and there are a few open source technologies available, namely:
\begin{itemize}
	\item OpenGL
	\item Pixie
	\item Java3D
	\item Allegro
\end{itemize}

The strongest candidate currently is Java because it has good library support for 3D rendering and highly usable GUIs. In addition, Java is platform independent. However, the other options will still be considered.

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
\section{PROBLEM SPECIFICATION}
\label{sec:problem}
The problem is defined by two main components, the first being an investigation into existing free EM solvers. The second is building an open source educational EM solver.
\subsection{Requirements} 
\label{reqs}
The requirements for the project are itemised below.
\begin{itemize}
	\item The survey must provide information on numerous packages, with a focus on feature that could stand out as educationally beneficial. Having an intuitive Graphical User Interface (GUI) and being free-ware or open source are examples.
	\item The program developed must be open source. This means that all operational components used within it (as well as all dependencies) should be open source as well.
	\item The program should not be built from scratch. At the very least an existing computational engine must be incorporated. If an existing open source solution is available, that may become the subject of this project such that it is improved (for the purpose of education).
	\item At the very least, an intuitive GUI must be incorporated into a functional solver for basic thin-wire antennas. Radiation patterns with beam-width values are necessary outputs for a frequency or wavelength input.
\end{itemize}
\subsection{Assumptions}
\label{assumps}
These assumptions allow for a more well defined project that can be judged successful with greater clarity.
 \begin{itemize}
 	\item To support the spirit of openness (and accessibility, especially for the academic world), the program should be cross platform. Windows, Linux and iOS should be able to run it.
 	\item The final product will not be the best option for running simulations in real world applications due to limited functionality. However, it should be able to accurately simulate thin wire models.
 	\item The best set of applications for education will be wire antennas, requiring thin wire modelling.
 	\item It is assumed that the survey and program can be completed before the Staff Inspection key date (14 October 2015). Also, it is assumed the project will start on 07 September 2015.
 \end{itemize}

\subsection{Constraints}
\label{consts}
The main constraint for this project will be time. Only five weeks can be given to the investigation and program (with the program taking four weeks). 

The software as an educational tool will be required to run on personal computers. This means that the algorithms should be effective and not use too many resources that it will not be viable to run on lower end computers. Also, it should be open source and platform independent, meaning that its components should be too (e.g. existing API).

\subsection{Success Criteria} %not bullet points
\label{succrit}
The project will be deemed a success if each of these points are satisfied.
\begin{itemize}
	\item A completed comparative survey on existing solvers focused on non-commercial packages.
	\item A stable open source and platform independent EM solver is developed, having at most minor bugs. 
	\item The developed program must use at least one existing numerical method geared towards solving wire antennas.
	\item The program must also be intuitive to use, as well as providing insight and understanding into electromagnetics that could be used in education.
\end{itemize}
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
\section{PROPOSED METHODOLOGY}
\label{sec:methodology}
A Yagi will be the benchmark antenna used in the survey since it is a thin-wire antenna with a certain degree of complexity. A dipole for example is a trivial solution and therefore cannot provide significant results\cite{radiationpatterns}. Specifically on on user friendly and accurate the program is. The packages will be assessed according to the following criteria:
\begin{itemize}
	\item Open Source (License)
	\item Graphical User Interface (GUI)
	\item Documentation
	\item Ease of Use
	\item Ease of Install
	\item Cross Platform (Win/Linux/iOS)
	\item Power of Electromagnetic Computations
	\item Computational Engine (numerical method)
	\item Benefits (and remarks)
\end{itemize}
A set of 17 electromagnetic solvers have been selected to test (these appear to be able to solve thin wire antennae problems). The list can be found in the Software Package table in Appendix \ref{app}. Most of these programs are free to use, or have limited editions which are.

If there is a suitable open source package available, it will be built upon to create a suitable teaching tool. Otherwise, an existing numerical method will be used in conjunction with a new GUI. The numerical method will be interfaced to the new GUI. Furthermore, a 3D rendering of the radiation patterns calculated will have to be coded. Note that all software used and produced should be open source, options are mentioned in Section \ref{tech}. The most likely numerical method to be implemented will be Method of Moments (MoM) due its applicability to radiation and coupling problems (see Section \ref{existsolns}).

Once basic functionality is achieved, additional features can be added. After which, debugging and feature improvements can take place. This will occur prior to demonstration at Open Day. Additional features will be largely inspired from the survey, incorporating highly intuitive designs to give good direction towards a quality output.

The explanations of the project phases found in the Gantt Chart can be found in Section \ref{divisionoflabour}.

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
\section{PROJECT MANAGEMENT}
\label{sec:projmang}

\subsection{Project Timeline}
\label{timeline}
The project timeline is shown as a Gantt chart found in Appendix \ref{app}. The work will primarily be completed between 7 September and 13 October where the project will be inspected by the school staff. To minimise the risk of delay with surveying the programs, the programs to be tested will be installed before 7 Sep, the official project start. There is time left for documentation after this inspection but before the electronic submission deadline. This means that actual work towards completing the project can be focused on before staff inspection without concern for low quality documentation. A presentation is also required after the electronic submission date and all work can be completed after that deadline. This allows for deliverables to be focused on sequentially.

Most of the work must be completed before Staff Inspection, as it will be inspected. This allows for time after Open Day to complete documentation (such as software documentation and reports). Also, there will be time after submission of documentation to prepare for the presentation. The project key dates are:

\begin{enumerate}
	\item Project Start - 7 September 2015
	\item Staff Inspection of Projects - 14 October 2015
	\item Project Open Day Presentation - 15 October 2015
	\item Electronic Project Report Submission - 23 October 2015
	\item Project Oral Presentation - 03 November 2015
\end{enumerate}

\subsection{Required Resources}
\label{reqresources}
The software packages that will be tested will defined as a resource to justify installing them before the official project start. Personal computers will also be required to run simulations, code and document. Since the survey will probably use simpler designs, super-computers (often used to run EM simulations) should not be required. 

\subsection{Division of Labour}
\label{divisionoflabour}
The tasks found in the Gantt Chart in Appendix \ref{app} are expounded on here, as well as how the work is divided between the engineer partners. A table showing which phases have been tasked to each engineer can also be found in Appendix \ref{app}. It should be noted that one engineer is of the IE discipline while the other is of the EE discipline.
\subsubsection{Investigation of Existing Software}
This is the starting point of the project and is part of both the literature review and deliverables. Various existing open source solvers will be investigated and contrasted by using the Software Package table found in Appandix \ref{app}. Each engineer will take half of the programs to investigate. They will work closely, comparing work and being aware of the other's work to minimise subjectivity. This is important to see what software already exists which is suitable for the teaching applications specified. It is however assumed that a basic stand-a-lone software package will still have to built regardless of what is found. What these findings might influence, are what features are included or prioritised in the EM solver.
\subsubsection{GUI}
One engineer will be responsible for `GUI' and `3D rendering' while the other will be responsible for `Backend and Interfacing'. The Graphical User Interface (GUI) is the most defining aspect of the program since it will determine how successful the program can be used as a teaching tool. Inspiration will be drawn from existing packages and will be given to the IE engineer.
\subsubsection{3D rendering}
OpenGL is an open source graphics API that will be used. Combining Java and OpenGL to create the 3D interface for drawing and visualising radiation patterns will be done in this step. It ties in with the GUI phase and will also be given to the IE engineer.
\subsubsection{Backend and interfacing}
The numerical method needed to run the solver will have to be acquired (and perhaps edited). Furthermore, the solver algorithm must then be interfaced with the front end such that information is correctly passed in and out. It is given to the EE engineer. It has been decided that the GUI will be written independently of how the numerical method inputs and outputs data, and it should be interfaced such that other methods could be incorporated later on.
\subsubsection{Additional Features}
These will add onto basic functionality to define the program as a teaching tool. Intuitive usability will be the focus along with components that provide intuitive insight into the simulations. Tools should allow the user to easily adapt various pre-built or user-built models to see how changes to voltage and physical dimensions affect radiation patterns. Additional features will be inspired by investigation and work division will have to occur at this step when more details are available.
\subsubsection{Bug Fixing and Open Day prep}
In order to ensure that the program is stable, time must be allocated for bug fixing and final touches. Open Day will showcase an exhibition of the project and therefore a stall must be designed and implemented. This will include posters and a computer with the program running to demonstrate its capabilities. Partners will perform bug fixes on the areas of the program they worked on. Both will be responsible for Open Day preparation but if the one partner is tied down with bugs then the other can take up the bulk of Open Day work.
\subsubsection{Documentation}
Formal documentation  will be left primarily to after the showcasing at Open Day. Each engineer will write individually for separate reports, but critical information and appendices may be shared. Documentation is very important if the program is to be available as an open source project and both will have to write up software documentation for their respective code.
\subsubsection{Presentation}
The presentation will be compiled and practised for the conference and submission milestones/deadlines. The point will be to present the work already completed and documented as a professional presentation. Both partners will be equally responsible and will work together.
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
\section{CONCLUSION}
\label{sec:conclusion}
A method for providing an open source educational tool in computational electromagnetics has been drawn up. Two engineers will first perform an investigation into existing free software packages. This will be followed by creating a basic solver with a basic GUI and 3D rendering scheme using an existing numerical method, preferably Method of Moments (MoM). Features will be added that were inspired from existing solutions. These features will promote understanding and provide an intuitive interface for learning. The project will be completed between 7 September 2015 and 03 November 2015. The program and survey should be completed by 14 October 2015.

\bibliographystyle{unsrt}
\bibliography{labprojref}

\clearpage \onecolumn
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
\appendices \pagestyle{empty}
\section{Tables and Gantt Chart} \label{app}
Figure \ref{fig:gantt} shows the Gantt chart for the combined workload of both engineers. Work within activities will be subdivided during execution between the partners except when allocated to only one engineer (see Table \ref{tab:labour}).

\begin{table}[H] \caption{Table Showing The Proposed Division of Labour Between Engineers\label{tab:labour}}
	\centering
	\begin{tabular}{l|c|c} 
		\hline \textbf{Task} & \textbf{Engineer} & \textbf{Time} \\ 
		\hline \hline Investigation of Existing Software   & 1 \& 2 & 1 week\tablefootnote{Not including installing the software, which should be mostly completed prior to project start} \\ 
		GUI  & 1 & 1 week \\ 
		3D Rendering   & 1 & 1 week \\ 
		Backend and Interfacing   & 2 & 2 weeks \\ 
		Additional Features  & 1 \& 2 & 1 week \\ 
		Bug Fixing and Open Day Prep   & 1 \& 2 & 1.5weeks \\ 
		Documentation   & 1 \& 2 & 1 week \\ 
		Presentation   & 1 \& 2 & 1.5 weeks \\ 
		\hline 
	\end{tabular} 
\end{table}

Figure \ref{fig:comptable} shows the table that will be filled in during the first stage of the project. It will be the final product of the software survey. This allows for easy comparison between the programs to determine which ones are outstanding in specific areas.
\begin{figure}[h] \centering
	\includegraphics[width=\textwidth]{include/comparisontable.png}
	\caption{Table to be used to compare software packages\label{fig:comptable}}
\end{figure}


\begin{landscape}
	\begin{figure}[h] \centering
		\begin{ganttchart}[
			hgrid,
			vgrid,
			x unit=3.5mm,
			time slot format=isodate]
			{2015-09-07}{2015-11-05}
			\gantttitlecalendar{year, month=name, day, week} \\
			\ganttbar{Investigation of existing software}{2015-09-07}{2015-09-13} \\
			\ganttbar{GUI}{2015-09-14}{2015-09-20} \\
			\ganttbar{3D rendering}{2015-09-21}{2015-09-27} \\ 
			\ganttbar{Backend and interfacing}{2015-09-14}{2015-09-27} \\
			\ganttbar{Additional features}{2015-09-28}{2015-10-04} \\
			\ganttbar{Bug fixing \& Open Day prep}{2015-10-05}{2015-10-13} \\
			%\ganttmilestone{Deadline for title changes}{2015-10-02} \ganttnewline
			\ganttmilestone{Staff inspection of projects}{2015-10-14} \ganttnewline
			\ganttmilestone{Open Day \& Tree planting}{2015-10-15} \ganttnewline
			\ganttbar{Documentation}{2015-10-16}{2015-10-22} \\
			\ganttmilestone{Electronic submission deadline}{2015-10-23} \ganttnewline
			\ganttbar{Presentation}{2015-10-24}{2015-11-01} \\
			\ganttmilestone{Presentation submission}{2015-11-02}
			
			\ganttlink{elem0}{elem1}
			\ganttlink{elem1}{elem2}
			\ganttlink[link mid=.16]{elem0}{elem3}
			\ganttlink[link mid=.75]{elem2}{elem4}
			\ganttlink{elem3}{elem4}
			\ganttlink{elem4}{elem5}
			\ganttlink{elem5}{elem6}
			\ganttlink{elem6}{elem7}
			\ganttlink{elem7}{elem8}
			\ganttlink{elem8}{elem9}
			\ganttlink{elem9}{elem10}
			\ganttlink{elem10}{elem11}
		\end{ganttchart}
		\caption{Gantt Chart for Open Source EM Solver\label{fig:gantt}}
	\end{figure}
\end{landscape}


\end{document}



